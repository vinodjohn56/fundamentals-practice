package com.fundamentals;

import com.fundamentals.Model.Login;
import com.fundamentals.Model.Person;
import com.fundamentals.Service.Exercises.ArrayExercises;
import com.fundamentals.Service.Exercises.DataTypeExercises;
import com.fundamentals.Service.Exercises.LoopExercises;
import com.fundamentals.Service.Exercises.StringExercises;
import com.fundamentals.Service.LoginServiceImpl;
import com.fundamentals.Service.PersonServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 0. Create Person model with id, name, address, birthdate, phone, email and create its object in main class.
 * 1. Create a Login model and its service to validate whether the user is correct.
 * 1A. For Validation, email should be matching with Person's email and default password(SDA_J@V@).
 * 2. Create a Login object in main class, print Login values in console.
 * 3. Pass Login values to Login Service. If login credentials are correct, then created Person's object values should be printed in the console along with Person's age . (edited)
 */
public class Practice {

    public static void main(String[] args) {
        Person person = new Person(1L, "Vinod", "Tallinn, Estonia", LocalDate.of(1990, 8, 20)
                , "+37245638228", "vinod@gmail.com");
        Person person1= new Person(1L, "Vinoddfsdfsdfsdf", "Tallinn, Estonia", LocalDate.of(1990, 8, 20)
                , "+37245638228", "vinoadd@gmail.com");

        List<Person> personList = new ArrayList<>();
        personList.add(person);
        personList.add(person1);

        Login login = new Login();
        login.setEmail("vinod@gmail.com");
        login.setPassword("1223");
        login.setPerson(personList);




        LoginServiceImpl loginService = new LoginServiceImpl();
        loginService.validateLogin(login, person);

        StringExercises stringExercises = new StringExercises();
        DataTypeExercises dataTypeExercises = new DataTypeExercises();

        //LoopExercises loopExercises = new LoopExercises();
        ArrayExercises arrayExercises = new ArrayExercises();



    }
}
