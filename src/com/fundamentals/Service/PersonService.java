package com.fundamentals.Service;

import com.fundamentals.Model.Person;

import java.time.LocalDate;

/**
 * Service to process Person entity
 */
public interface PersonService {
    /**
     * To print person details
     * @param person
     */
    void printPerson(Person person);

    /**
     * To calculate age from Birth date
     * @param birthDate
     * @return
     */
    int calculateAgeFromBirthDate(LocalDate birthDate);
}
