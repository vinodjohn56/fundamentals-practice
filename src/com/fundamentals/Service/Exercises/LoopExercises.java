package com.fundamentals.Service.Exercises;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class LoopExercises {
    public LoopExercises() {
/*        multiplicationTable(7);
        printEvenNumbers(1, 100);
        printRecursiveNumber(5);
        printDivisableBy3and5();*/
        printPyramid(5);
        printPyramids(5);
    }

    private void multiplicationTable(int number) {
        System.out.println("\n\nExercise-1:");
        for (int i = 1; i <= 10; i++) {
            System.out.println(number + " x " + (i) + " = " + (number * (i)));
        }
    }

    private void printEvenNumbers(int min, int max) {
        System.out.println("\n\nExercise-2:");
        for (int i=min; i<=max; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }

    private void printRecursiveNumber(int number) {
        System.out.println("\n\nExercise-3:");
        System.out.print(number);
        for (int i = 2; i <= number; i++) {
            System.out.print(" ");
            for (int j=1; j<=i; j++) {
                System.out.print(number);
            }
        }
    }

    private void printDivisableBy3and5() {
        List<Integer> dividableThree = new ArrayList<>();
        List<Integer> dividableFive = new ArrayList<>();
        List<Integer> dividableBoth = new ArrayList<>();
        int[] arr = {};
        int i = 1;
        while (i <= 100) {
            if (i % 3 == 0) {
                dividableThree.add(i);
            }
            if (i % 5 == 0) {
                dividableFive.add(i);
            }
            if (i % 3 == 0 && i % 5 == 0) {
                dividableBoth.add(i);
            }
            i++;
        }
        System.out.println("These are dividable with 3: " + dividableThree);
        System.out.println("These are dividable with 5: " + dividableFive);
        System.out.println("These are dividable with both: " + dividableBoth);
    }

    private List<Integer> getPrimeNumbers(int maxNumber) {
        int primeNumbersCount = 1;
        int numberToCheck = 2;
        List<Integer> primeNumList = new ArrayList<>();
        while (primeNumbersCount <= maxNumber) {
            if (isPrime(numberToCheck)) {
                primeNumbersCount++;
                primeNumList.add(numberToCheck);
            }
            numberToCheck++;
        }
        return primeNumList;
    }

    private static boolean isPrime(int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    private List<Integer> getOddNumbers() {
        List<Integer> oddNumList = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            if (i % 2 != 0) {
                oddNumList.add(i);
            }
        }
        return oddNumList;
    }
    private void printPyramid(int maxNumber) {
        List<Integer> oddArray = getOddNumbers();
        for(int i=1; i<=maxNumber; i++) {
            for (int j = 1; j <= maxNumber - i; j++) {
                System.out.print(" ");
            }
            for(int j=1; j<= oddArray.get(i-1); j++) {
                System.out.print(i);
            }
            System.out.println("\n");
        }
    }

    private void printPyramids(int maxnumber) {
        int it = 1;
        for (int i = 1; i <= maxnumber; i++) {
            for (int j = 1; j <= maxnumber - i; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < it; j++) {
                System.out.print(i);
            }
            System.out.println("");
            it += 2;
        }
    }
}


