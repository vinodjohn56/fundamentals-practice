package com.fundamentals.Service.Exercises;

public class StringExercises {

    public StringExercises() {
        printName();
        printJavaImage();
        printToLowerCase();
        reverseWord();
        printFirstHalfOfWord();
        concatenateSubstrings();
        printLengthOfPhrase();
        compareIgnoreCases();
    }

    private void printName() {
        System.out.println("\n\nExercise 1:");
        System.out.println("Hello\nVinod");
    }

    private void printJavaImage() {
        System.out.println("\n\nExercise 2:");
        System.out.println("    J      a      v       v       a");
        System.out.println("    J   a     a    v     v     a     a");
        System.out.println("J   J  a a a a a    v   v     a a a a a");
        System.out.println(" JJ   a         a     v      a         a");
    }

    private void printToLowerCase() {
        System.out.println("\n\nExercise 3:");
        String word = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
        System.out.println(word + "->" + word.toLowerCase());
    }

    private void reverseWord() {
        System.out.println("\n\nExercise 4:");
        String word = "Java", result = "";
        word = word.replaceAll(".", "$0 ");
        String[] wordArr = word.split(" ");
        for (int i = (wordArr.length -1); i >= 0; i--) {
            result += wordArr[i];
        }
        System.out.println(result);
    }

    private void printFirstHalfOfWord() {
        System.out.println("\n\nExercise 5:");
        String word = "Programmer";
        System.out.println(word.substring(0, (word.length()/2) - 1));
    }

    private void concatenateSubstrings() {
        System.out.println("\n\nExercise 6:");
        String word1 = "Java", word2 = "Fundamentals";
        System.out.println(word1.substring(1).concat(word2.substring(1)));
    }

    private void printLengthOfPhrase() {
        System.out.println("\n\nExercise 7:");
        String words = "This is Java!";
        System.out.println(words.length());
    }

    private void compareIgnoreCases() {
        System.out.println("\n\nExercise 8:");
        String word1 = "This is a comparison";
        String word2 = "THIS is A Comparison";

        System.out.println("WORD1: " + word1 + "||" + "WORD2: " + word2);
        if(word1.equalsIgnoreCase(word2)) {
            System.out.println("Both words are same");
        } else {
            System.out.println("Both words are different");
        }
    }




}
