package com.fundamentals.Service.Exercises;

public class DataTypeExercises {
    public DataTypeExercises() {
        printCircleStats();
    }

    private void printCircleStats() {
        double radius = 7.5;
        double area = Math.PI * Math.pow(radius, 2);
        double perimeter = 2 * Math.PI * radius;
        System.out.println("Area = " + area);
        System.out.println("Perimeter = " + perimeter);
    }
}
