package com.fundamentals.Service.Exercises;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayExercises {
    public ArrayExercises() {
        System.out.println("Array Exercises");
        printSumOfArray();
        printAverageOfArray();
        printOddArray();
        printMinAndMax();
        printReverseArray();
        printOddAndEvenOfArray();
        insertNewElementInArray();
        printSecondLargest();
    }

    private int getSumOfArray(int[] arr) {
        int result = 0;
        for (int i = 0; i < arr.length; i++) {
            result += arr[i];
        }
        return result;
    }

    private void printSumOfArray() {
        System.out.println("\n\nExercise-1:");
        int[] arr = {1, 7, 3, 10, 9};
        System.out.println(getSumOfArray(arr));
    }

    private void printAverageOfArray() {
        System.out.println("\n\nExercise-2:");
        int[] arr = {1, 7, 3, 10, 9};
        System.out.println(getSumOfArray(arr) / arr.length);
    }

    private void printOddArray() {
        System.out.println("\n\nExercise-3:");
        int[] arr = {1, 7, 3, 10, 9};
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                System.out.print(arr[i] + ",");
            }
        }
    }

    private void printMinAndMax() {
        System.out.println("\n\nExercise-4:");
        int[] arr = {1, 7, 3, 10, 9};
        Arrays.sort(arr);
        System.out.println("Minimum of array:" + arr[0]);
        System.out.println("Maximum of array:" + arr[arr.length - 1]);

    }

    private void printReverseArray() {
        System.out.println("\n\nExercise-5:");
        int[] arr = {1, 7, 3, 10, 9};
        int[] arr2 = new int[5];
        int j = 0;
        for (int i = arr.length - 1; i >= 0; --i) {
            arr2[j] = arr[i];
            j++;
            /* System.out.print(arr[i] + ",");*/
        }
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + ",");
        }
    }

    private void printOddAndEvenOfArray() {
        System.out.println("\n\nExercise-6:");
        int numberOfOdds = 0, numberOfEvens = 0;
        int[] arr = {1, 7, 3, 10, 9};
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                numberOfEvens += 1;
            } else {
                numberOfOdds += 1;
            }
        }
        System.out.println("Number of Odd numbers: " + numberOfOdds);
        System.out.println("Number of Even numbers: " + numberOfEvens);
    }

    private void insertNewElementInArray() {
        System.out.println("\n\nExercise-7:");
        int[] arr = {1, 7, 3, 10, 9};
        int[] arr2 = new int[6];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = (i == 2) ? 4 : ((i > 2) ? arr[i-1] : arr[i]);
        }

        System.out.println("\n\nBefore:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ",");
        }

        System.out.println("\n\nAfter:");
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + ",");
        }
    }

    private void printSecondLargest() {
        System.out.println("\n\nExercise-9:");
        int[] arr = {1, 7, 3, 10, 9};
        Arrays.sort(arr);
        System.out.println("Second Largest number: " + arr[arr.length - 2]);
    }
}
