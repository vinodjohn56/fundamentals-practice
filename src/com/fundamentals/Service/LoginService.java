package com.fundamentals.Service;

import com.fundamentals.Model.Login;
import com.fundamentals.Model.Person;

/**
 * Service to handle login related operations
 */
public interface LoginService {

    /**
     * To check the login details matches the person.
     *
     * @param login
     * @param person
     */
    void validateLogin(Login login, Person person);
}
