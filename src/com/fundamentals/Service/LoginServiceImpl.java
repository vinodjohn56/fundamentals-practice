package com.fundamentals.Service;

import com.fundamentals.Model.Login;
import com.fundamentals.Model.Person;

/**
 * Implementation of Login Service
 */
public class LoginServiceImpl implements LoginService {
    private String DEFAULT_LOGIN_PASSWORD = "SDA_J@V@";

    @Override
    public void validateLogin(Login login, Person person) {
        System.out.println("LOGIN DETAILS:");
        System.out.println("E-MAIL: " + login.getEmail());
        System.out.println("PASSWORD: " + login.getPassword());
        if(login.getEmail().equals(person.getEmail()) && login.getPassword().equals(DEFAULT_LOGIN_PASSWORD)) {
            PersonServiceImpl personService = new PersonServiceImpl();
            personService.printPerson(person);
        }
    }
}
