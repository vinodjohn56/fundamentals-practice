package com.fundamentals.Service;

import com.fundamentals.Model.Person;

import java.time.LocalDate;
import java.util.Date;

/**
 * Implementation of Person Service
 */
public class PersonServiceImpl implements PersonService {
    @Override
    public void printPerson(Person person) {
        System.out.println("PERSON DETAILS:");
        System.out.println("ID: " + person.getId());
        System.out.println("NAME:" + person.getName());
        System.out.println("ADDRESS:" + person.getAddress());
        System.out. println("BIRTH DATE:" + person.getBirthDate());
        System.out.println("E-MAIL:" + person.getEmail());
        System.out.println("PHONE:" + person.getPhone());
        System.out.println("AGE:" + calculateAgeFromBirthDate(person.getBirthDate()));
    }

    @Override
    public int calculateAgeFromBirthDate(LocalDate birthDate) {
        return LocalDate.now().getYear() - birthDate.getYear();
    }
}
