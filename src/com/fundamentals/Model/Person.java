package com.fundamentals.Model;

import java.time.LocalDate;

/**
 * Person Entity
 */
public class Person {
    private Long id;
    private String name;
    private String address;
    private LocalDate birthDate;
    private String phone;
    private String email;

    public Person(Long id, String name, String address, LocalDate birthDate, String phone, String email) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.phone = phone;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }
}
