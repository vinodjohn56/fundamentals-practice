package com.fundamentals.Model;

import java.util.List;

/**
 * Login entity
 */
public class Login {
    private String email;
    private String password;
    private List<Person> person;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Person> getPerson() {
        return person;
    }

    public void setPerson(List<Person> person) {
        this.person = person;
    }
}
